/*Condiciones CASE
Busca en la fila que le indiquemos y Cuando encuentre un 90 mostrara un valor, cuandoe encuentre un 80 un valor diferente
*/
SELECT last_name,department_id,
CASE department_id
WHEN 90 THEN 'Management'
WHEN 80 THEN 'Sales'
WHEN 60 THEN 'It'
ELSE 'Other dept.'
END AS "Department"
FROM employees;

/*Es la misma idea que un CASE*/
SELECT last_name,department_id,
DECODE(department_id,
90, 'Management',
80, 'Sales',
60, 'It',
'Other dept.')
AS "Department"
FROM employees;

/**/