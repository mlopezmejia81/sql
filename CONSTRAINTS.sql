/*CONSTRAINTS*/
/*Son reglas que se aplican a las tablas*/
/*Cada CONSTRAINT debe de tener un nombre definido despues de la KEYWORD*/
CREATE TABLE clients
(client_number NUMBER(4) CONSTRAINT clients_cient_num_pk PRIMARY KEY,
last_name VARCHAR2(13) CONSTRAINT clients_last_name_nn NOT NULL,
email VARCHAR2(80) CONSTRAINT clients_emil_uk UNIQUE);

/*Se pueden asignar CONSTRAINT  a nivel campo o a nivel tabla*/
CREATE TABLE clients (
client_number NUMBER(6) NOT NULL,
first_name VARCHAR2(20),
last_name VARCHAR2(20),
phone VARCHAR2(20),
email VARCHAR2(10) NOT NULL,
/*--->*/CONSTRAINT clients_phone_email_uk UNIQUE (email,phone));

/*Un UNIC CONSTRAINT es unico, por ejemplo cuando lo ponemos en un email, este debe de ser unico en toda la tabla*/


/*Un email se puede repetir o un telefono, pero no ambos*/
CONSTRAINT clients_phone_email_uk UNIQUE(email,phone)

/*Llave primaria compuesta
se puede usar en un historial de trabajo que ha trabajado en varios lugares
entocnes en la tabal de jobs se podria repetir su employee_id, o se podria repetir la misma fecha de comienzo, pero no al mismo tiempo los dos*/
CREATE TABLE copy_job_history
(employee_id NUMBER(6,0),
start_date DATE,
job_id VARCHAR2(10),
department_id NUMBER(4,0),
CONSTRAINT copy_jhist_id_st_date_pk PRIMARY KEY(employee_id, start_date));

/*FOREING KEY*/
CREATE TABLE copy_employees
(employee_id NUMBER(6,0) CONSTRAINT copy_emp_pk PRIMARY KEY,
first_name VARCHAR2(20),
last_name VARCHAR2(25),
department_id NUMBER(4,0) CONSTRAINT c_emps_dept_id_fk
REFERENCES departments(department_id),
email VARCHAR2(25));

/*ON DELETE SET NULL*/
/*sI BORRAMOS A LA CLASE PADRE, NO BORRARA A LOS HIJOS PERO BORRARA SUS PRIMARY KEY*/



/*CHECK*/
/*ASEGURAR CON CHECK QUE LA FECHA DE FIN SEA MAYOR A LA DE INICIO*/
/*No hay limite del numero de CHECKS que poodemos usar*/
CREATE TABLE copy_job_history
(employee_id NUMBER(6,0),
start_date DATE,
end_date DATE,
job_id VARCHAR2(10),
department_id NUMBER(4,0),
CONSTRAINT cjhist_emp_id_st_date_pk
PRIMARY KEY(employee_id, start_date),
CONSTRAINT cjhist_end_ck CHECK (end_date > start_date));
/*
� Column-level syntax:*/
salary NUMBER(8,2) CONSTRAINT employees_min_sal_ck CHECK (salary > 0)
� /*Table-level syntax:*/
CONSTRAINT employees_min_sal_ck CHECK (salary > 0)




/*Modificar CONSTRAINTS*/
ALTER TABLE employees
MODIFY (email CONSTRAINT emp_email_nn NOT NULL);

/*Borrar CONSTRAINT*/
ALTER TABLE copy_departments
DROP CONSTRAINT c_dept_dept_id_pk CASCADE;


/*Desactivar un CONSTRINT*/
/*Cuando deshabilitas y despues habilitas todos los CONTRAINTS que dependian del que deshabilitaste no se activaran solos, tienes que activarlos manualmente*/
CREATE TABLE copy_employees
( employee_id NUMBER(6,0) PRIMARY KEY DISABLE,
...
...);
ALTER TABLE copy_employees
DISABLE CONSTRAINT c_emp_dept_id_fk;

/*Con este comando puedes ver todos los CONSTRAINTS de la tabla*/
SELECT*
FROM USER_CONSTRAINTS
WHERE table_name ='EMPLOYEES';
