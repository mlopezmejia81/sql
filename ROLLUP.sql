/*GROUP BY ROLLUP
agrupa primero por department_id y job_id y despues saca el salario
el subtotal es el ultimo valor que se encuentra en la tercera columna antes de que el ID cambie, el ultimo valor es el total de los subtotales por job_id
*/
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY ROLLUP (department_id, job_id);

/*GROUP BY CUBE
La diferencia de este es que da el subtotal de job_id y por separado por department_id, sin importar si el job_id no tenga department_id*/
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);

/*GROUPING SETS
En un solo SELECT hace 3 agrupaciones para no tener que hacer muchos SELECTs*/
SELECT department_id, job_id, manager_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY GROUPING SETS
((job_id, manager_id), (department_id, job_id), (department_id, manager_id));

select manager_id, sum(salary)
from employees
where manager_id=100  
group by manager_id;


/*GROUPING
Cuando en una columna hay un NULL pondra un 1 y cuando haya informacion pondra un 0
se tiene que indicar en el GROUPING que columna*/
SELECT department_id, job_id, SUM(salary),
GROUPING(department_id) AS "Dept sub total",
GROUPING(job_id) AS "Job sub total"
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);