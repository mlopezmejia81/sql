
/*Hacemos una relacion de una tabla a si misma, creamor dos alias como si tuvieramos 2 tablas, donde buscamos que el manager del empleado se relacione con el ID de un 
empleado
en este caso el empleado King no tiene ejfe porque es el dueno
Puede haber un manager que no tenga empleados o un empleado que no trabaje para un manager*/
SELECT NVL(worker.manager_id,0),NVL(worker.employee_id,0), worker.last_name || ' works for ' || manager.last_name
AS "Works for"
FROM employees worker FULL OUTER JOIN employees manager
ON (worker.manager_id = manager.employee_id);

/*Aqui mostramos el manager de cad aempleado, pero agregue el FULL OUTER para mostrar todos los datos aunque no cumplan con la condicion*/
SELECT worker.last_name, worker.manager_id, manager.last_name
AS "Manager name"
FROM employees worker FULL OUTER JOIN employees manager
ON (worker.manager_id = manager.employee_id);

/*Aqui se construye un arbol de la empresa, muestra al nombre del manager y quie trabaja para el, ya que termina de mostrar sigue con el siguiente manager que trabaja para king y muestra 
quien trabaja para el*/
SELECT employee_id, last_name, job_id, manager_id
FROM employees
START WITH employee_id = 100
CONNECT BY PRIOR employee_id = manager_id;

/*el CONNECT BY PRIOR se usa para indicar cual sera la prioridad para hacer la consulta*/
SELECT last_name ||' reports to ' || PRIOR last_name AS "Walk Top Down"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR employee_id = manager_id;

SELECT LEVEL, last_name ||
' reports to ' ||
PRIOR last_name
AS "Walk Top Down"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR
employee_id = manager_id;


/*LEVEL nos da el nivel de la columna*/
SELECT LEVEL,LPAD(last_name, LENGTH(last_name)+
(LEVEL*2)-2,'_') AS "Org_Chart"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR employee_id = manager_id;

/*esta sentencia nos muestra el arbol pero de forma inversa, al colocar el PRIOR del lado derecho del = es el que hace el cambio */
SELECT employee_id, LPAD(last_name, LENGTH(last_name) + (LEVEL*2)-2, '_') AS
ORG_CHART
FROM employees
START WITH last_name = 'Grant'
CONNECT BY employee_id = PRIOR manager_id;

/*Aqui se corta la ramma donde el apellido sea Zlotkey*/
SELECT employee_id, LPAD(last_name, LENGTH(last_name) + (LEVEL*2)-2, '_') AS
ORG_CHART
FROM employees
WHERE last_name != 'Zlotkey'
START WITH last_name = 'Grant'
CONNECT BY employee_id = PRIOR manager_id;


/*Estas dos sentencias hacen loi mismo pero no es de manera tan visual*/
SELECT last_name
FROM employees
WHERE last_name != 'Higgins'
START WITH last_name = 'Kochhar'
CONNECT BY PRIOR employee_id = manager_id; 

SELECT last_name
FROM employees
START WITH last_name = 'Kochhar'
CONNECT BY PRIOR employee_id = manager_id
AND last_name != 'Higgins';
