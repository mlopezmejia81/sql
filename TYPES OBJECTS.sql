
/*CREACION DE OBJECTOS*/
/*** Using user-defined types (UDTs) in SQLJ ***/
/*** Create ADDRESS UDT ***/
CREATE TYPE ADDRESS AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
)

CREATE TYPE PERSON AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    ADDRESS
)
/*** Create a typed table for PERSON objects ***/
CREATE TABLE persons OF PERSON
/*** Create a relational table with two columns that are REFs 
     to PERSON objects, as well as a column which is an Address ADT. ***/
CREATE TABLE  employees_2
( 
  empnumber            INTEGER PRIMARY KEY,
  person_data     REF  PERSON,
  manager         REF  PERSON,
  office_addr          ADDRESS,
  salary               NUMBER
)
/*** Insert some data--2 objects into the persons typed table ***/
INSERT INTO persons VALUES (
            PERSON('Wolfgang Amadeus Mozart', 123456,ADDRESS('Am Berg 100', 'Salzburg', 'AT','10424')));
INSERT INTO persons VALUES (
            PERSON('Ludwig van Beethoven', 234567,ADDRESS('Rheinallee', 'Bonn', 'DE', '69234')))    ;                 
               
/** Put a row in the employees table **/
INSERT INTO employees_2 (empnumber, office_addr, salary) VALUES ( 1001,ADDRESS('500 Oracle Parkway', 'Redwood Shores', 'CA', '94065'),
            50000);
/** Set the manager and PERSON REFs for the employee **/
UPDATE employees_2
   SET manager =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Wolfgang Amadeus Mozart');

UPDATE employees_2
   SET person_data =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven');
       
/*
Note:
Use of a table alias, such as p above, is a recommended general practice in Oracle SQL, especially in 
accessing tables with user-defined types.*/
       
       
       
       
       
       