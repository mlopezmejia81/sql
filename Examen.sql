
EXAMEN PRIMERA SEMANA BECA SQL
1) �Qu� hace la siguiente consulta?
SELECT MAX(salary) "Salario Mayor", MIN(salary) "Salario menor", AVG(salary) "Salario
Promedio", SUM(Salary) "TOTAL Salarios"
FROM employees
WHERE hire_date <= '01/01/04' AND department_id = 50;

R: 
Selecciona el salario maximo con el alias "Salario Mayor", el salario minimo con el alias "Salario menor", el promedio del salario con el alias "Salario
Promedio", y la suma de los salarios con el alias "TOTAL Salarios", de la tabla de empleados con la condicion de que la fecha de contratacion "hire_date"sea menor a
'01/01/04' pero solo en el departamento cuyo id sea igual a 50.

2) Realiza una consulta para conocer la comisi�n m�s alta y la comisi�n promedio de los
empleados, pero solamente del departamento 80. Cada Columna debe ir con su respectivo
alias.
R:
SELECT MAX(commission_pct) "Comision mas alta", AVG(commission_pct) "Promedio de las comisiones"
FROM employees
WHERE department_id=80

3) Crea una tabla Estudiantes, con un campo para el nombre, apellido paterno, apellido
materno, edad, y fecha de nacimiento, el campo fecha deber� de tener un valor default null,
y el apellido paterno no deber� permitir valores nulos.

R:
CREATE TABLE estudiantes_examen
(estudiante_id NUMBER(10) CONSTRAINT id_student_pk PRIMARY KEY NOT NULL,
estudiante_nombre VARCHAR2(20) NULL,
estudiante_a_paterno VARCHAR(20) NOT NULL,
estudiante_a_materno VARCHAR(20) NULL,
estudiante_fecha_ingreso DATE DEFAULT NULL);

4) �Qu� hace el siguiente script?
CREATE TABLE emp_load (
employee_number CHAR(5),
employee_dob CHAR(20),
employee_last_name CHAR(20),
employee_first_name CHAR(15),
ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY def_dir1
ACCESS PARAMETERS (RECORDS DELIMITED BY NEWLINE FIELDS
(employee_number CHAR(2), employee_dob CHAR(20), employee_last_name CHAR(18),
employee_first_name CHAR(11), "mm/dd/yyyy")) LOCATION ('info.dat'));

R:
-Crea una tabla llamada "emp_load" con los siguientes campos:
employee_number de tipo CHAR con longitud de 5,
employee_dob de tipo CHAR con longitud de 20,
employee_last_name de tipo CHAR con longitud de 20,
employee_first_name de tipo CHAR con longitud de 15,
-Despues con "ORGANIZATION EXTERNAL" le indica a Oracle es una tabla externa
-Con "TYPE ORACLE_LOADER" es un producto de Oracle 
-Con "DEFAULT DIRECTORY def_dir1" le indica que el nombre del directorio sera "def_dir1"
-Con "ACCESS PARAMETERS" indicara como leer el archivo
-Con "RECORDS DELIMITED BY NEWLINE" sirve para indicar como identificar el inicio y fin de un a fila
-Con "FIELDS" indica el nombre y tipo de datos de los campos


5) �Cu�l es la diferencia entre ambos registros?

09-JUL-2020 05.42.30.000000 PM
09-JUL-2020 05.42.30.567945 PM

R: La primera fecha es de tipo DATE ya que no muestra las fracciones de segundo y la segunda es de tipo TIMESTAMP ya que si muestra las fracciones de segundo

6) Escribe el c�digo para agregar una columna comida_favorita a la tabla Estudiantes con
valor default null
R:
ALTER TABLE estudiantes
ADD(comida_favorita VARCHAR2(20) DEFAULT NULL);

7) �Qu� hace la siguiente l�nea?

ALTER TABLE nombre_tabla SET UNUSED nombre_columna;

R:Deshabilita una columna llamada "nombre_tabla"

8) Muestra en una consulta el nombre, apellido, nombre de departamento de todos los
empleados, si no tiene departamento asignado muestra "No asignado".

R:
SELECT e.first_name "Nombre", e.last_name "Apellido", nvl(d.department_name,'**NO ASIGNADO**') "Departamento"
FROM employees e LEFT OUTER JOIN departments d
ON (e.department_id=d.department_id)
ORDER BY d.department_id


9) Muestra nombre, apellido, empleo actual, puestos anteriores y fecha de fin de cada
puesto para todos los empleados que fueron contratados entre 2003 y 2005.

R:
SELECT e.first_name "Nombre", e.last_name "Apellido", j.job_id "Empleo Actual", j.end_date "Fecha de fin"
FROM employees e LEFT OUTER JOIN job_history j ON(e.employee_id=j.employee_id)
WHERE EXTRACT(YEAR FROM e.hire_date) BETWEEN 2003 AND 2005
ORDER BY e.employee_id;

10) MUESTRA EL ID DE TRABAJADOR, LAST NAME, ID DE MANAGER ORDENADOS POR ID
DE MANAGER (selfjoin)

R:
SELECT e.employee_id, e.last_name, e.manager_id "ID Jefe",m.last_name "Jefe"
FROM employees e JOIN employees m 
ON(e.manager_id=m.employee_id)
ORDER BY e.manager_id

11) MUESTRA ID DE TRABAJADOR, LAST NAME, ID DE MANAGER, LAST NAME DE
MANAGER CON UN QUERY DE JERARQUIA (El primer eslabon es el employee_id=100) Y
ORDENARLOS POR EL ID DEL MANAGER (join de jerarquia)

R:
SELECT employee_id, last_name, manager_id, PRIOR last_name as "Apellido Manager"
FROM employees
START WITH employee_id=100
CONNECT BY PRIOR employee_id=manager_id
ORDER BY PRIOR employee_id;

12) Explica lo que realiza la siguiente sentencia:
SELECT AVG (DISTINCT salary)
FROM employees
WHERE department_id = 30;

R:
Selecciona el promedio de los distintos salarios de los trabajadores del departamento 30

13) Escribe la sentencia donde se muestre cu�ntos sueldos diferentes se pagan a los
empleados del departamento 50 de tabla empleados.
R:

SELECT COUNT(DISTINCT salary) "Salarios diferentes"
FROM employees
WHERE department_id=50;

14) Escribe la sentencia para obtener el nombre, apellido, el puesto y el salario de los
empleados que tienen un salario m�nimo mayor a $5,000, y ord�nalo por salario.

R:
SELECT first_name, last_name, job_title, salary
FROM employees NATURAL JOIN jobs
WHERE min_salary >5000
ORDER BY salary

15) Describe lo que realiza la siguiente sentencia.
SELECT department_name, street_address, country_name
FROM departments JOIN locations USING (location_id) JOIN countries USING (country_id)
ORDER BY department_name;

R:
Selecciona el nombre del departamento, la direccion de la calle y el nombre de la ciudad de 3 diferentes tablas haciendo uso de 2 JOINs y usando las llavs que tienes en comun dichas tablas
y los ordena de acuerdo al nombre del departamento

16) Se requiere una tabla para realizar el registro de inscripci�n de participantes a un
concurso, donde se requiere un numero de boleto que ser� �nico y se registraran el nombre
y apellido, hasta un m�ximo de 50 participantes. Crear la tabla, la secuencia y agregar al
menos 2 participantes.

R:

CREATE TABLE registro
(registro_boleto NUMBER(2,0) CONSTRAINT boleto_pk PRIMARY KEY,
registro_nombre VARCHAR2(20),
registro_apellido VARCHAR2(20));

CREATE SEQUENCE registro_secuencia /*Nombre de la secuencia*/
INCREMENT BY 1 /*Se incrementara de 1 en 1*/
START WITH 1/*Comenzara con el 1*/
MAXVALUE 50 /*el maximo valor de registros seera de 50*/
NOCACHE
NOCYCLE;

INSERT INTO registro
(registro_boleto, registro_nombre, registro_apellido)
VALUES (registro_secuencia.NEXTVAL, 'Angel', 'Lopez');/*NEXTVAL indica que se tiene que tomar el siguiente valor en la lista de la secuencia*/

INSERT INTO registro
(registro_boleto, registro_nombre, registro_apellido)
VALUES (registro_secuencia.NEXTVAL, 'Miguel', 'Mejia');/*NEXTVAL indica que se tiene que tomar el siguiente valor en la lista de la secuencia*/

17) Explica la siguiente l�nea de SQL:
CREATE SYNONYM respaldo FOR copy_employees;

R:
Crea un sinonimo de una tabla llamada "copy_employees" y lo llama como "respaldo", esto es muy util cuando tenemos nombres de tablas muy largos o dificiles de recordar
y que con el sinonimo podemos obtener todos los datos de esa tabla 

18) Define una tabla con el nombre "estudiantes", con los siguientes campos y sus
caracter�sticas:
1.* "estudiante_id" -> con longitud de 5 y que no sea NULL
2.* "estudiante_nombre" -> con longitud de 20 y que no sea NULL
3.* "estudiante_apellido_paterno" -> con longitud de 20 y que permita valores NULL
4.* "estudiante_apellido_materno" -> con longitud de 20 y que permita valores NULL
5.* "estudiante_numero" -> con longitud de 15 que permita valores NULL
Y que contenga un CONSTRAINT con el nombre "uk_estudiantes" a nivel tabla en donde se
indique que el "estudiante_apellido_paterno" y "estudiante_apellido_materno" tienen que
ser �nicos en toda la tabla, haciendo uso de la keyword UNIQUE y mostrar que
efectivamente se cre� que CONSTRAINT para la tabla.

CREATE TABLE estudiantes (
estudiante_id NUMBER(5) NOT NULL,
estudiante_nombre VARCHAR2(20) NOT NULL,
estudiante_apellido_paterno VARCHAR2(20) NULL,
estudiante_apellido_materno VARCHAR2(20) NULL,
estudiante_numero NUMBER(15),
/*---> A nivel tabla*/CONSTRAINT uk_estudiantes UNIQUE (estudiante_apellido_paterno,estudiante_apellido_materno));


SELECT*
FROM USER_CONSTRAINTS
WHERE table_name ='ESTUDIANTES';


19) Crea una tabla llamada "jefes" con un id, una fecha de contrataci�n y de finalizaci�n, el
id de la escuela y el id regional de la escuela, agregando un CONSTRAINT a la
fecha_contratacion para que no sea NULL, crea un CONSTRAINT a nivel tabla donde se
define una PRIMARY KEY compuesta por dos campos y al final compara que la fecha de
finalizaci�n sea mayor a la de contrataci�n

R:
CREATE TABLE jefes
(jefes_id NUMBER(6,0),
fecha_contratacion DATE CONSTRAINT final_cstr_jefes NOT NULL,
fecha_finalizacion DATE,
escuela_id VARCHAR2(10),
escuela_regional_id NUMBER(4,0),
CONSTRAINT jefes_id_pk PRIMARY KEY(jefes_id, fecha_contratacion),
CONSTRAINT jefes_check CHECK (fecha_finalizacion > fecha_contratacion));

20) Consulta el apellido, convi�rtelo a may�sculas y reemplaza las A may�sculas por -,
concat�nalos con el nombre en min�sculas, usa la tabla de employees.

R:
SELECT ( REPLACE ( UPPER(last_name) , 'A', '-' ) ) || ' ' || LOWER(first_name) "Cambio"
FROM employees;

21) �Qu� hace la siguiente consulta?
SELECT department_id, manager_id, AVG(salary)
FROM employees GROUP BY ROLLUP (department_id, manager_id);

R:
Selecciona el id del departamento, el id del manager, y el promedio del salario de la tabla empleados y los agrupa por "department_id, manager_id"
agrupandolos por subtotales


22) Se desea generar una vista que contenga los siguientes datos: -first_name, last_name,
employee_id y salary y que sea con los registros que tengan un salario entre $17,000 y
$3,200

R:
CREATE VIEW view_salary
AS SELECT  employee_id, first_name, last_name, salary
FROM employees
WHERE salary BETWEEN 17000 AND 32000

Con los datos anteriores se requiere mostrar a los empleados ordenados por salario.
SELECT *
FROM view_salary
ORDER BY salary


23) �Qu� datos se obtienen de hacer la siguiente uni�n de tablas?
SELECT employees.last_name, departments.department_name
FROM employees, departments;

R:
Se obtiene un producto cartesiano ya que no se especifican las condiciones para unir las tablas


24) Considerando una lista de empleados y una lista de departamentos. �De qu� forma es
posible visualizar si cada empleado est� asignado o no a un departamento?

R:
SELECT last_name, NVL(e.department_id,0)
FROM employees e, departments d
WHERE e.department_id =d.department_id(+);

25) �Qu� hace la siguiente consulta?
SELECT LAST_DAY( (SELECT MAX(HIRE_DATE)
FROM EMPLOYEES
WHERE HIRE_DATE<SYSDATE))
FROM DUAL;

R:
Selecciona el ultimo dia del mes de la fecha de contratacion de la tabla empleados, cuando la fecha de contratacion sea menor a la fecha del servidor

26) �Qu� hace la siguiente consulta?
SELECT HIRE_DATE, EMPLOYEE_ID, JOB_ID, TO_DATE(NULL)
FROM EMPLOYEES
WHERE EMPLOYEE_ID>(SELECT MIN(EMPLOYEE_ID) FROM employees)
UNION SELECT START_DATE,EMPLOYEE_ID,JOB_ID, END_DATE
FROM JOB_HISTORY
WHERE JOB_ID < (SELECT MAX(JOB_ID) FROM JOB_HISTORY)
ORDER BY EMPLOYEE_ID;

R:
Regresa "HIRE_DATE, EMPLOYEE_ID, JOB_ID" de la tabla employees con la condicion de que el ID del empleado sea mayor a el id menor de los empleados
hace una union de esa tabla con la de job_history de la cual regresa "START_DATE,EMPLOYEE_ID,JOB_ID, END_DATE" con la condicion de que el ID  de job sea menor al ID mayor de esa tabal

27) Dadas las tablas Job_history y jobs, encuentre los Job_id que se intersecan.
R:

SELECT job_id
FROM job_history
INTERSECT 
SELECT job_id
FROM jobs ;

28) Si quiero saber los nombres de empleados que fueron contratados despu�s de Peter
Vargas y no tengo la informaci�n de la fecha de contrataci�n de Peter, �C�mo realizar�a esa
consulta? , escribe el c�digo:

R:

SELECT *
FROM employees
WHERE hire_date>(SELECT hire_date
FROM employees
WHERE last_name = 'Vargas')
ORDER BY hire_date

29) �Qu� realiza la siguiente consulta?
SELECT last_name, salary
FROM employees
WHERE salary < (SELECT AVG(salary) FROM employees);

R:
Selecciona el apellido y salario de la tabla empleados con la condicion de que el salario sea menor al promedio del salario de los empleados


30) �Qu� devuelve la consulta?
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN
(SELECT EXTRACT(YEAR FROM hire_date) FROM employees WHERE
department_id=90);

R:
Regresa el apellido y fecha de contratacion de la tabla empleados con la condicion de que el anno de la fecha de contratacion este dentro de los annos de la fecha de contratacion del 
departamento 90, el SELECT anidado puede regresar mas de una fila

31) �Qu� hace la siguiente consulta?
SELECT last_name, salary, NVL2(commission_pct, salary + (salary * commission_pct),salary) "Ganancia Total"
FROM employees
WHERE department_id IN(80,90);

R:
Regresa el apellido, salario, y en el NVL2 especifica que si "commission_pct" != NULL entonces nos regresara "salary + (salary * commission_pct)", pero si es NULL entonces
nos regresara el salario normal 

32) De la tabla employees, realiza un query que asigne un nombre a un departamento, debe
cumplir con lo siguiente:
Si el campo department_id = 90 asignale 'Gerencia' si el campo department_id = 80 asignale
'Ventas' si el campo department_id = 60 asignale 'Caja' si no pertenece a ninguno asiganale
la palabra OTRO

R:
SELECT department_id,
CASE department_id
WHEN 90 THEN 'Gerencia'
WHEN 80 THEN 'Ventas'
WHEN 60 THEN 'Caja'
ELSE 'Other dept.'
END AS "Cambio Nombre"
FROM employees

33) �Qu� hace la siguiente consulta?
SELECT department_id, job_id, count(*) AS conteo
FROM employees
WHERE department_id > 10
GROUP BY department_id, job_id
ORDER BY conteo;

R:
Regresa el "department_id, job_id" de la tabla empleados con la condicion de que el department_id sea mayor a 10 y cuenta cuantos job_id hay por departamento
y los agrupa por "department_id, job_id" ordenandolos de acuerdo al conteo 

34) Desarrolla el c�digo que agrupe por departamentos de la tabla empleado y obtenga el
salario m�ximo de cada uno de los grupos, a su vez cuente las veces que se repite cada uno
de los departamentos, y �nicamente muestre aquellos datos en el que el conteo fue mayor a
2 empleados por departamento. Finalmente, los datos sean ordenados por el salario m�ximo
de forma ascendente.

R:
SELECT department_id, MAX(salary), count(*) AS conteo
FROM employees
GROUP BY department_id
HAVING count(*) > 2
ORDER BY MAX(salary);

35) �Qu� har� esta sentencia y bajo qu� condiciones?
INSERT ALL
WHEN department_id IN (50) THEN
INTO all_50s VALUES (employee_id, last_name, salary, department_id)
WHEN department_id IN (90)
THEN INTO all_90s VALUES (employee_id, last_name, salary, department_id)
WHEN department_id IN (60)
THEN INTO all_60s VALUES (employee_id, last_name, salary, department_id)
SELECT employee_id, last_name, salary, department_id FROM employees;

R:
Instertara los valores en all_50s cuando el id del departamento sea 50, despues Instertara los valores en all_90s cuando el id del departamento sea 90
Instertara los valores en all_60s cuando el id del departamento sea 60, seleccionando "employee_id, last_name, salary, department_id" de la tabla de empleados


36) �Qu� hace la siguiente sentencia merge?
MERGE INTO backup_employees be USING employees e
ON (be.employee_id = e.employee_id)
WHEN MATCHED THEN UPDATE
SET be.salary = e.salary * 1.5
WHERE e.salary < (SELECT ROUND(AVG(salary))
FROM employees
WHERE department_id = e.department_id)

WHEN NOT MATCHED THEN INSERT
VALUES (e.employee_id, e.first_name, e.last_name, e.email, e.phone_number,
e.hire_date, e.job_id, e.salary, e.commission_pct, e.manager_id, e.department_id);

R:
Insterta o actualiza valores en una tabla, inserta un salario en la tabla "backup_employees" igual al salario de "employees" pero multiplicado por 1.5
pero solo cuando el salario sea menor al promedio redondeado del salario de los empleados por departamento
