/**Valor DEFAULT*/
INSERT INTO my_employees
(hire_date, first_name, last_name)
VALUES
(DEFAULT, 'Angelina','Wright');

/*Se puede ver de manera imlpicita*/
INSERT INTO my_employees
(first_name, last_name)
VALUES
('Angelina','Wright');

/*UPDATE con DEFAULT*/
UPDATE my_employees
SET hire_date = DEFAULT
WHERE last_name = 'Wright';

/*MERGE
modifica los registros de la tabla, si el registro existe le aplica un UPDATE 
pero si el registro no existe entonces lo crea*/
MERGE INTO copy_employees c USING employees e
ON (c.employee_id = e.employee_id)
WHEN MATCHED THEN UPDATE
    SET
    c.last_name = e.last_name,
    c.department_id = e.department_id
WHEN NOT MATCHED THEN 
INSERT (employee_id, first_name, last_name, email,phone_number, hire_date,
job_id, salary)
VALUES (e.employee_id, e.first_name, e.last_name, e.email, e.phone_number, e.hire_date,
e.job_id, e.salary);


/*INSTERT ALL
inserta datos en dos tablas en unsa sola sentencia*/
INSERT ALL
    INTO my_employees
    VALUES (hire_date, first_name, last_name)
    
    INTO copy_my_employees
    VALUES (hire_date, first_name, last_name)
SELECT hire_date, first_name, last_name
FROM employees;

/*Aqui condicionamos la insercion de datos a cada tabla, por medio de un WHERE usando el formato
format IN ('','','')*/
INSERT ALL
WHEN call_ format IN ('tlk','txt','pic') THEN
INTO all_calls
VALUES (caller_id, call_timestamp, call_duration, call_format)
WHEN call_ format IN ('tlk','txt') THEN
INTO police_record_calls
VALUES (caller_id, call_timestamp, recipient_caller)
WHEN call_duration < 50 AND call_type = 'tlk' THEN
INTO short_calls
VALUES (caller_id, call_timestamp, call_duration)
WHEN call_duration > = 50 AND call_type = 'tlk' THEN
INTO long_calls
VALUES (caller_id, call_timestamp, call_duration)
SELECT caller_id, call_timestamp, call_duration, call_format,
recipient_caller
FROM calls
WHERE TRUNC(call_timestamp ) = TRUNC(SYSDATE);

/*INSERT FIRST*/
/*Cuando encuentre el primer WHEN en donde se cumple la condicion entonces lo aplica y se salta todos los demas*/