/*Foramtos de fechas*/
SELECT hire_date, TO_CHAR(hire_date, 'fmMonth dd, YEAR') AS Nueva, TO_CHAR(hire_date, 'Month dd, YYYY') AS Nueva2, TO_CHAR(hire_date, 'fmMonth ddth, YYYY') AS nueva3,
TO_CHAR(hire_date, 'fmDay ddth Mon,YYYY') AS Nueva4, TO_CHAR(hire_date, 'fmDay ddthspMon, YYYY') AS Nueva5
FROM employees;

/*Formatos de horas*/
SELECT TO_CHAR(SYSDATE, 'hh:mm')
FROM dual;

/*Formatos de numeros*/
SELECT salary, TO_CHAR(salary,
'$99,999') AS "Salary",  TO_CHAR(salary, '99,999.99') AS Salary2, TO_CHAR(salary, '0009999') AS Salary3
FROM employees;

/*Numeros a cadena*/
SELECT TO_NUMBER('5,320', '9,999'), 
AS "Number"
FROM dual;

SELECT TO_NUMBER('4350', '9999')
AS "Bonus"
FROM dual;

/*Conversion de fechas*/
SELECT TO_DATE('May10,1989', 'fxMonDD,YYYY') AS "Convert", TO_DATE('Sep 07, 1965', 'fxMon dd, YYYY') AS
"Date",TO_DATE('July312004', 'fxMonthDDYYYY') AS
"Date2",TO_DATE('June 19, 1990','fxMonth dd, YYYY') AS
"Date3",  TO_DATE('27-Oct-95','DD-Mon-YY')
AS "Date"
FROM DUAL;

SELECT last_name,hire_date, TO_CHAR(hire_date, 'DD-Mon-YY') AS conversion
FROM employees
WHERE hire_date < TO_DATE('01-Jan-03','DD-Mon-YY'); 