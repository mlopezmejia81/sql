
/*Pdemos usar dos formas para instertar, pero la mas recomendable es la primera que se meustra a continuacion*/
INSERT INTO copy_departments
(department_id, department_name, manager_id, location_id)
VALUES
(200,'Human Resources', 205, 1500);


select *
from copy_departments;

/*Segunda forma*/
INSERT INTO copy_departments
VALUES
(210,'Estate Management', 102, 1700);

DESCRIBE copy_departments;

/*Se pueden isntertar valores vacios solo en campos que asi lo permiten*/
INSERT INTO employees
(employee_id, first_name, last_name, email, phone_number,
hire_date, job_id, salary)
VALUES
(302,'Grigorz','Polanski', 'gpolanski', '', '15-Jun-2017',
'IT_PROG',4200);

select *
from employees
where employee_id=302;

/*La otra forma de instertar*/
INSERT INTO employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(304,'Test',USER, 't_user', 4159982010, SYSDATE, 'ST_CLERK',2500);

select *
from employees
where employee_id=304;

SELECT first_name, TO_CHAR(hire_date,'Month, fmdd, yyyy') as nueva_fecha
FROM employees
WHERE employee_id = 101;


/*Instertando valores de fecha especificos con TO_DATE */
INSERT INTO employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(305,'Pepe','Hernandez', 'loepz','8586667641',
TO_DATE('July 8, 2017', 'Month fmdd, yyyy'), 'MK_REP',4200); 

INSERT INTO employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(309,'Test',USER, 't_user', 4159982010, SYSDATE, 'ST_CLERK',2500);

/*Aqui nos dan una fecha y una hora de contratacion pero debemos de convertirla al formato de la tabla
despues con el SELECT mostramos la fecha que aunque cambio su formato si conservo su informacion*/
INSERT INTO employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(303,'Angelina','Wright', 'awright','4159982010',
TO_DATE('July 10, 2017 17:20', 'Month fmdd, yyyy HH24:MI'),
'MK_REP', 3600); 

SELECT first_name, last_name,
TO_CHAR(hire_date, 'dd-Mon-YYYY HH24:MI') As "Date and Time"
FROM employees
WHERE employee_id = 303;

/*Aqui insertamos en una nueva tabal las columnas del SELECT que tenag en job_id las letras REP*/
INSERT INTO sales_reps(id, name, salary, commission_pct)
SELECT employee_id, last_name, salary, commission_pct
FROM employees
WHERE job_id LIKE '%REP%';

/*Aqui insertamos todo lo que tiene employees en sales_reps, aunque deben de tener la misma estructura*/
INSERT INTO sales_reps
SELECT *
FROM employees;


/*UPDATE
Actualizar un valor de un campo*/
UPDATE employees
SET phone_number = '123456'
WHERE employee_id = 303;

UPDATE employees
SET phone_number = '654321', last_name = 'Jones'
WHERE employee_id >= 303;

UPDATE employees
SET salary = 
    (SELECT salary
    FROM copy_employees
    WHERE employee_id = 100)
WHERE employee_id = 101;


UPDATE copy_employees
SET salary = 
    (SELECT salary
    FROM copy_employees
    WHERE employee_id = 205),
job_id = 
    (SELECT job_id
    FROM copy_employees
    WHERE employee_id = 205)
WHERE employee_id = 206;

/*aCTUALIZAR UNA TABALA DESDE OTRA TABLA */
UPDATE copy_employees
SET salary = 
    (SELECT salary
    FROM employees
    WHERE employee_id = 205)
WHERE employee_id = 202;


ALTER TABLE copy_employees
ADD (department_name varchar2(30) );

CREATE TABLE copy_employees
AS (SELECT * FROM employees);

UPDATE copy_employees e
SET e.department_name = (SELECT d.department_name
FROM departments d
WHERE e.department_id = d.department_id);

/*DELETE*/
DELETE FROM copy_employees
WHERE department_id =
(SELECT department_id
FROM departments
WHERE department_name = 'Shipping');

/*Elimina de copy empleados */
DELETE FROM copy_employees e
WHERE e.manager_id IN
                    (SELECT d.manager_id
                    FROM employees d
                    HAVING count (d.department_id) < 2
                    GROUP BY d.manager_id);

/*SALE ERROR PORQUE EL id 999 NO EXISTE Y LAST_NAME NO ACEPTA NULL*/
UPDATE copy_employees
SET last_name = (SELECT last_name
FROM copy_employees
WHERE employee_id = 999)
WHERE employee_id = 101;

UPDATE copy_employees
SET department_id = 10
WHERE department_id = 20;

UPDATE copy_employees
SET department_id = 15
WHERE employee_id = 100;

/*Cuando aplicas un FOR UPDATE, lo que ingresas en el SELECT seran las columnas que se bloquearan para otros
usuarios de tu base de datos hasta que apliques un COMMIT o un ROLLBACK*/
SELECT e.employee_id, e.salary, d.department_name
FROM employees e JOIN departments d USING (department_id)
WHERE job_id = 'ST_CLERK' AND location_id = 1500
FOR UPDATE
ORDER BY e.employee_id;