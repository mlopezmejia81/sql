/*ALTER TABLE
Agrega un DEFAUL a un campo que ya existe*/
ALTER TABLE my_cd_collection
ADD (release_date DATE DEFAULT SYSDATE);


/*SI LA TABLA QUE YA ESTA CREADA LE AGREGA UNA COLUMNA*/
ALTER TABLE my_friends
ADD (favorite_game VARCHAR2(30));


/*Creamos una tabla con una longuitud de nombre de 20
despues la alteramos */
CREATE TABLE mod_emp
(last_name VARCHAR2(20),
salary NUMBER(8,2));

/*Aqui no tendriamos problemas porque la longuitud a la que aumentaremos es mayor*/
ALTER TABLE mod_emp
MODIFY (last_name VARCHAR2(30));

/*Aqui habria un problema ya que si el campo ya esta lleno y el nombre mas largo tiene mas de 10 caracteres va a haber problemas al quere 
ponerle un alonguitud mas peque;a*/
ALTER TABLE mod_emp
MODIFY (last_name VARCHAR2(10));

/*aqui no hay problema porque se incrementa el numero de numeros que se pueden ingresar*/
ALTER TABLE mod_emp
MODIFY (salary NUMBER(10,2));


/*Aqui asignamos que tenga un valor DEFAULT de valor de 50*/
ALTER TABLE mod_emp
MODIFY (salary NUMBER(8,2) DEFAULT 50);

/*Borrar columnas*/
ALTER TABLE my_cd_collection
DROP COLUMN release_date;

ALTER TABLE my_friends
DROP COLUMN favorite_game;


/*SET UNUSED
Marcar una columna como no usada*/
ALTER TABLE tablename SET UNUSED (column name);

ALTER TABLE copy_employees
SET UNUSED (email);

/*Eliminar columnas que no se usan*/
ALTER TABLE copy_employees
DROP UNUSED COLUMNS;

/*DROP TABLE
Eliminar tabla*/
DROP TABLE copy_employees;
DROP TABLE my_friends;

FLASHBACK TABLE my_friends TO BEFORE DROP;
/*FLASHBACK TABLE*/
/*Recuperar tabla borrada por accidente*/
FLASHBACK TABLE tablename TO BEFORE DROP;

FLASHBACK TABLE copy_employees TO BEFORE DROP;

/*user_recyclebin*/
/*Si eliminas una tabla esta se guardara en el USER_RECYCLEBIN*/
SELECT original_name, operation, droptime
FROM user_recyclebin

/*PURGE*/
/*Si eliminas un a tabla con este comando ya nunca podras recuperarla ya que no pasara por la papelera*/
DROP TABLE copy_employees PURGE;

/*RENAME
para cambiar el nombre de un a tabla*/
RENAME old_name to new_name;
RENAME my_cd_collection TO my_music;

/*TRUNCATE*/
/*Es igual que el DELETE pero con la diferencia en que con TRUNCATE no se puede hacer un rollback, por lo cual es mas rapdio pero esta
es una tecnica definitiva para borrar los contenidos de una columna*/
TRUNCATE TABLE tablename;

/*COMMENT ON TABLE*/
/*Comentarios en una tabla*/
COMMENT ON TABLE tablename | COLUMN table.column
IS 'place your comment here';
COMMENT ON TABLE employees
IS 'Western Region only';

/*Muestra los comentarios de una tabla*/
SELECT table_name, comments
FROM user_tab_comments;

/*Si queremos quitar el comentario a una tabla le ponemos un comentario vacio*/
COMMENT ON TABLE employees IS ' ' ;


/*ORACLE TIENE SU PROPIO CONTROLADOR DE VERSIONES
puedes cambiar alg en una tabla y despues de ejecutar el COMMIT
podemos ver que cambios se hicierona a la tabla*/
INSERT INTO copy_employees
VALUES (1, 'Natacha', 'Hansen', 'NHANSEN', '4412312341234',
'07-SEP-1998', 'AD_VP', 12000, null, 100, 90, NULL); 

SELECT employee_id,first_name ||' '|| last_name AS "NAME",
versions_operation AS "OPERATION",
versions_starttime AS "START_DATE",
versions_endtime AS "END_DATE", salary
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;

UPDATE copy_employees
SET salary = 1
WHERE employee_id = 1;

/*Con esta sentencia pueden ver que cambios se aplicaron a la tabla*/
SELECT employee_id,first_name ||' '|| last_name AS "NAME",
versions_operation AS "OPERATION",
versions_starttime AS "START_DATE",
versions_endtime AS "END_DATE", salary,
VERSIONS_XID as "quien"
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;

DELETE from copy_employees
WHERE employee_id = 1;

SELECT employee_id,first_name ||' '|| last_name AS "NAME",
versions_operation AS "OPERATION",
versions_starttime AS "START_DATE",
versions_endtime AS "END_DATE", salary
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;





