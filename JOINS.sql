/*NATURAL JOIN 
une tablas sin ponerle que columnas deben de ser, 
lo malo es que los campos deben de ser del mismo tipo de dato y llamarse igual*/
SELECT first_name, last_name, job_id, job_title, min_salary
FROM employees NATURAL JOIN jobs
WHERE department_id > 80;

/*Otra manera de usa un JOIN es indicar un alias a cada tabla, y en el SELECT indicar el alias.columna, y en el WHERE le ponemos la condicion donde sean iguales 
las columnas que deseemos, el alias se define en el FROM
el JOIN es entre una primary key con una foreing key*/
SELECT e.first_name, e.last_name, e.job_id, j.job_title, j.min_salary
FROM employees e, jobs j
WHERE e.job_id = j.job_id 
AND department_id > 80;

/*CROSS JOIN 
Hace un producto cartesiano, mul;tiplica una columna por toda la informacion de otra
*/
SELECT last_name, department_name
FROM employees CROSS JOIN departments;

/*aqui si se identifica el campo que deben de coincidir las tablas, el nombre de las tablas tiene que ser el mismo en ambas tablas
en caso de que el campo no sea el mismo, tendriamos que usar la expresion utilizada en donde definimos un alias para cada tabla*/
SELECT first_name, last_name, department_id, department_name
FROM employees JOIN departments USING (department_id);

/*en este condicionamos el valor */
SELECT first_name, last_name, department_id, department_name
FROM employees JOIN departments USING (department_id)
WHERE last_name = 'Higgins';

/*Esta es la manera correcta de realizarlo, solo es necesario poner el alias en el SELECT en la columna que comparten ambas tablas
se pueden poner el alias de cualquiera de las tablas, puede ser e.job_id o j.job_id
Si ya se asigno una alias ya no se pueden asignar el nombre de las tablas*/
SELECT last_name, e.job_id, job_title
FROM employees e, jobs j
WHERE e.job_id = j.job_id
AND department_id = 80;

/*Usamos un ON cuando los campos de las tablas no tienen el mismo nombre, dentro del ON se ponen las columnas que queremos comparar*/
SELECT last_name, job_title
FROM employees e JOIN jobs j
ON (e.job_id = j.job_id);


/*Si hacemos una union tenemos que tener cuidado, o podemos hacer un CROSS JOIN lo que generara valores falsos al hacer un producto cartesiano*/
SELECT last_name, job_title
FROM employees e, jobs j;

/*JOIN con condicion*/
SELECT last_name, job_title
FROM employees e JOIN jobs j
ON (e.job_id = j.job_id)
WHERE last_name LIKE 'H%';


/*Seleccionara las filas donde el sal;ario este entre el mas alto y el mas bajo, a partir del salario de un empleado le asigna un grado*/
SELECT last_name, salary, grade_level, lowest_sal, highest_sal
FROM employees JOIN job_grades
ON(salary BETWEEN lowest_sal AND highest_sal); 


/*JOIN de tres tablas, une la tabla empleados con el departamento, y del departamento obtiene la locacion*/
SELECT last_name, department_name AS "Department", city
FROM employees JOIN departments USING (department_id)
JOIN locations USING (location_id);




