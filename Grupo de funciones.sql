SELECT MIN(hire_date)
FROM employees;

SELECT MAX(hire_date)
FROM employees;

SELECT SUM(salary)
FROM employees
WHERE department_id = 90;

/*Promedio
Cuando se haga el promedio los campos que tengan NULL no seran considerados*/
SELECT ROUND(AVG(salary), 2)
FROM employees
WHERE department_id = 90;

/*Calcular que tanto los datos dse desvian de la media*/
SELECT ROUND(VARIANCE(life_expect_at_birth),4)
FROM wf_countries;

/*Es parecido a la variancia solo que esta elevado al cuadrado */
SELECT ROUND(STDDEV(life_expect_at_birth), 4)
FROM wf_countries;

SELECT MAX(salary), MIN(salary), MIN(employee_id)
FROM employees
WHERE department_id = 60;

/*cuenta los registros sin consideran lso nulos*/
SELECT COUNT(job_id),COUNT(*),COUNT(commission_pct)
FROM employees;

SELECT COUNT(*)
FROM employees
WHERE hire_date < '01-Jan-1996';

/*DISTINCT
Elimina las filas repetidas, solo retorna los IDs una vez*/
SELECT DISTINCT job_id
FROM employees;

/*Lo que no se repite ahora es la combinacion de las dos filas*/
SELECT DISTINCT job_id,
department_id
FROM employees;

/*Suma los salarios que no sean iguales*/
SELECT SUM(DISTINCT salary)
FROM employees
WHERE department_id = 90;

/*Cuenta cuantos distintos trabajos hay*/
SELECT COUNT (DISTINCT job_id)
FROM employees;

/*Cuenta cuantos salarios diferentes hay*/
SELECT COUNT (DISTINCT salary)
FROM employees;

/*Cambia los NULL por 0 y despues saca el promedio*/
SELECT AVG(NVL(commission_pct, 0))
FROM employees;

/*Gracias al GROUP BY, agrupa por ID  de departamento y saca el promedio por ID de departamento y despues los ordena por ID 
los campos que pongo en el SELECT no deben de afectar la agrupacion*/
SELECT department_id, AVG(salary)
FROM employees
GROUP BY department_id
ORDER BY department_id;

/*Obtiene el salario maximo de cada departamento*/
SELECT MAX(salary), department_id, ROUND(AVG(salary))
FROM employees
GROUP BY department_id
ORDER BY department_id;

/*cuenta los paises que hay por region*/
SELECT COUNT(country_name), region_id
FROM wf_countries
GROUP BY region_id
ORDER BY region_id;

/*Cuenta los registros*/
SELECT COUNT(*), region_id
FROM wf_countries
GROUP BY region_id
ORDER BY region_id;

SELECT department_id, MAX(salary)
FROM employees
WHERE last_name != 'King'
GROUP BY department_id;

SELECT region_id, ROUND(AVG(population)) AS population
FROM wf_countries
GROUP BY region_id
ORDER BY region_id;


/*Cuenta los empleados que hay por departamento y JOB_ID*/
SELECT department_id, job_id, count(*)
FROM employees
WHERE department_id > 40
GROUP BY department_id, job_id
ORDER BY department_id;

/*Cuenta los departamentos que tienen por JOB_ID*/
SELECT job_id, COUNT(department_id)
FROM employees
GROUP BY job_id;

/*Obtiene el maximo de cada salario por departamento*/
SELECT max(salary)
FROM employees
GROUP by department_id;

/*Obtiene el maximo de los promedios de todos los departamentos*/
SELECT max(avg(salary))
FROM employees
GROUP by department_id
ORDER BY department_id;

SELECT avg(salary),department_id
FROM employees
GROUP by department_id;

/*HAVING 
Encuentra el salario maximo de los departamento donde exista mas de un empleado
Primero se agrupan los registtros
luego las funciones agrupadas se palican 
al ultimo solo los grupos agrupados que cumplen con la clausala de HAVING se muestran*/
SELECT department_id,MAX(salary), COUNT(*)
FROM employees
GROUP BY department_id
HAVING COUNT(*)>1
ORDER BY department_id;

select *
from employees;

/*EJERCICIOS*/
/*Aqui regreso con esta consulta el numero de empleados que tiene cada manager a exepcion de los que su ID de manager es menor a 90*/
SELECT manager_id, count(*) as Empleados_Por_Manager
FROM employees
GROUP BY manager_id 
HAVING manager_id >90
ORDER BY manager_id;

/*Cuenta cuantos trabajadores hay por cada departamento*/
SELECT department_id, COUNT(*) as Trabajadores_Por_Departamento
FROM employees JOIN departments USING(department_id)
GROUP BY department_id
ORDER BY COUNT(*);

/*Cuenta las ciudades que tiene cada region*/
SELECT region_id as ID_de_Pais, count(*) as Ciudades_Por_Pais
FROM countries JOIN regions USING(region_id)
GROUP BY region_id;

SELECT *
FROM countries JOIN regions USING(region_id);


/*
Muestra el id de departamento y el salario promedio m�s alto
de ellos.
*/
SELECT
    department_id,
    AVG(salary) AS promedio
FROM
    employees
WHERE
    ROWNUM = 1
GROUP BY
    department_id
ORDER BY
    promedio DESC;
