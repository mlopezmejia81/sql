/*DEFINICION DE INDEX*/
CREATE INDEX index_name
ON table_name( column...,column)


CREATE INDEX cont_reg_id_idx
ON countries(region_id);

CREATE INDEX emps_name_idx
ON employees(first_name, last_name);


SELECT DISTINCT ic.index_name, ic.column_name,
ic.column_position, id.uniqueness
FROM user_indexes id, user_ind_columns ic
WHERE id.table_name = ic.table_name
AND ic.table_name = 'EMPLOYEES';

/*Cuando creamos un indece, lo que hacemos es que la velocidad de busqueda donde pusimos la funcion aumente*/
CREATE INDEX upper_last_name_idx
ON employees (UPPER(last_name));
 
SELECT *
FROM employees
WHERE UPPER(last_name) LIKE 'KIN%';
/*----------*/
CREATE INDEX emp_hire_year_idx
ON employees (TO_CHAR(hire_date, 'yyyy'));

SELECT first_name, last_name, hire_date
FROM employees
WHERE TO_CHAR(hire_date, 'yyyy') = '2001'
/*-------------*/
CREATE INDEX upper_first_name_idx
ON employees (UPPER(first_name));

SELECT *
FROM employees
WHERE UPPER(first_name) LIKE 'ST%';


/*Borrar indices*/
DROP INDEX upper_first_name_idx;



/*SYNONYM*/
CREATE SYNONYM amy_emps
FOR amy_copy_employees;

/*Borrar sinonimo*/
DROP SYNONYM amy_emps;
