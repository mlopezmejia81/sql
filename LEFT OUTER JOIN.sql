/*inner JOIN regresa los registros que haces match
Outer JOIN regresa los valores aunque no hagan match
aqui nos regresa los empleados aunque no tengan asignados un departamento */
SELECT e.last_name, d.department_id, d.department_name
FROM employees e LEFT OUTER JOIN departments d
ON (e.department_id = d.department_id);


/*NO regresa la columna que esta a la derecha del JOIN*/
SELECT e.last_name, d.department_id, d.department_name
FROM employees e RIGHT OUTER JOIN departments d
ON (e.department_id = d.department_id);

/*Este trae tanto lo que esta del lado derecho del JOIN como del lado izquierdo*/
SELECT NVL(e.last_name,0), d.department_id, NVL(d.department_name,0)
FROM employees e FULL OUTER JOIN departments d
ON (e.department_id = d.department_id);

SELECT last_name, e.job_id AS "Job", jh.job_id AS "Old job", end_date
FROM employees e LEFT OUTER JOIN job_history jh
ON(e.employee_id = jh.employee_id);