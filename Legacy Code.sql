SELECT last_name AS Apellido, city AS Ciudad
FROM employees e, departments d, locations l
WHERE e.department_id = d.department_id
AND d.location_id = l.location_id;

SELECT last_name, salary, grade_level, lowest_sal, highest_sal
FROM employees, job_grades
WHERE (salary BETWEEN lowest_sal AND highest_sal);

/*Aqui es un RIGHT OUTER JOIN*/
SELECT e.last_name, d.department_id,
d.department_name
FROM employees e, departments d
WHERE e.department_id(+)=
d.department_id;
/*Aqui es un LEFT OUTER JOIN*/
SELECT e.last_name, d.department_id,
d.department_name
FROM employees e, departments d
WHERE e.department_id=
d.department_id(+);