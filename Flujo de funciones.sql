/*Se evaluan de la interna a la externa*/
/*
1.-Primero Evalua ADD_MONTHS
2.-Despues NEXT_DAY
3.-Por ultimo TO_CHAR

AGREGA 6 MESES, DE AHI NOS DICE CUAL ES LA FECHA DEL SIGUIENTE VIERNES DESPUES DE AGREGAR LOS 6 MESES Y POR ULTIMO ESA FECHA LA CONVIERTE A CHAR
*/
SELECT TO_CHAR(NEXT_DAY(ADD_MONTHS(hire_date, 6), 'FRIDAY'), 'fmDay,
Month ddth, YYYY') AS "Next Evaluation"
FROM employees
WHERE employee_id = 100;

/*NULL NVL*/
SELECT commission_pct, NVL(commission_pct,0) AS SinNulos
FROM employees;

SELECT last_name,commission_pct,department_id, NVL(commission_pct, 0)*250
FROM employees
WHERE department_id IN(80,90);

/*NVL2*/
/* 
"if expression 1 has a value, substitute expression 2; if
expression 1 is null, substitute expression 3."*/
SELECT last_name, salary,commission_pct,
NVL2(commission_pct, salary + (salary * commission_pct), salary)
AS income
FROM employees
WHERE department_id IN(80,90);

/*NULL IF
Compara dos epxresiones, cuando son iguales nos regresa la primera expresion, si no son iguales nos regresa un null, SOLO QUE AQUI APLICAMOS EL NVL ANIDADO */
SELECT first_name, LENGTH(first_name) AS "Length FN", last_name,
LENGTH(last_name) AS "Length LN", NVL(NULLIF(LENGTH(first_name),
LENGTH(last_name)),0) AS "Compare Them"
FROM employees;

/*COALESCE
Busca entre dos expresiones un NULL, si la primera es NULL, entonces muestra la segunda expresion, si la segunda es NULL, muestra la primera, si ambas son NULL, 
muestra el valor que le indiquemos en los parentesis
*/
SELECT last_name,commission_pct,manager_id,
COALESCE(commission_pct, manager_id, 10)
AS "Comm"
FROM employees
ORDER BY commission_pct;
