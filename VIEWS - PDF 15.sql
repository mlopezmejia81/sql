/*VISTAS*/
/*Se usan para evitar comandos muy grandes*/
CREATE OR REPLACE VIEW view_employees
AS SELECT employee_id,first_name, last_name, email
FROM employees
WHERE employee_id BETWEEN 100 and 180;

select *
from view_employees

/*Aqui hacemos un UPDATE de los nombres de las columnas*/
CREATE OR REPLACE VIEW view_euro_countries
AS SELECT country_id AS "ID", country_name AS "Country",
capitol AS "Capitol City"
FROM wf_countries
WHERE location LIKE '%Europe';

/*Otra forma de hacerlo*/
CREATE OR REPLACE VIEW view_euro_countries("ID", "Country",
"Capitol City")
AS SELECT country_id, country_name, capitol
FROM wf_countries
WHERE location LIKE '%Europe';


/*Vistas complejas entre dos tablas*/
CREATE OR REPLACE VIEW view_euro_countries
("ID", "Country", "Capitol City", "Region")
AS SELECT c.country_id, c.country_name, c.capitol, r.region_name
FROM wf_countries c JOIN wf_world_regions r
USING (region_id)
WHERE location LIKE '%Europe';

/*READ ONLY
Una vista de solo lectura*/
CREATE OR REPLACE VIEW view_dept50
AS SELECT department_id, employee_id, first_name, last_name, salary
FROM employees
WHERE department_id = 50
WITH READ ONLY;

/*Borrar vista*/
DROP VIEW viewname;

/*InLine VIEWS*/
/*Nos regresa quien tiene el salario maximo por departamento*/
SELECT e.last_name, e.salary, e.department_id, d.maxsal
FROM employees e,
                (SELECT department_id, max(salary) maxsal
                FROM employees
                GROUP BY department_id) d
WHERE e.department_id = d.department_id
AND e.salary = d.maxsal
ORDER BY department_id;


/*ROWNUM 
Condiciona los resultados que le agregamos
Toma los primeros 5 registros y despues los los ordena por fecha de contratacion
Limita la consulta por los empleados que son los mas viejos en la empresa
*/
SELECT ROWNUM AS "Longest employed", last_name, hire_date
FROM employees
WHERE ROWNUM <=5
ORDER BY "Longest employed";

/*Este si regresa primero los empleados ordenados por fecha de registro
despues con el ROWNUM selecciona 5 de esos registros*/
SELECT ROWNUM AS "Longest employed", last_name, hire_date
FROM (SELECT last_name, hire_date
FROM employees
ORDER BY hire_date)
WHERE ROWNUM <=5
ORDER BY hire_date





