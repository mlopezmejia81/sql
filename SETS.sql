
/*Union de tablas cuando comparten datos iguales*/


/*UNION 
regresa los datos en ambas tablas y elimina los que sean iguales*/
SELECT a_id
FROM a
UNION
SELECT b_id
FROM b; 

/*UNION ALL
regresa todos los datos que tienen ambas tablas */
SELECT a_id
FROM a
UNION ALL
SELECT b_id
FROM b; 

/*INTERSECT
Regresa los datos que vomparten ambas tablas*/
SELECT a_id
FROM a
INTERSECT
SELECT b_id
FROM b; 

/**/
SELECT a_id
FROM a
MINUS
SELECT b_id
FROM b; 

/*Prueba de todas las funciones de arriba en nuestras tablas*/
select department_id
from employees
/*INTERSECT*/ /*UNION*/ /*UNION ALL*/ MINUS
select department_id
from departments;

/*TO_CHAR(NULL),TO_DATE(NULL), TO_NUMBER(NULL)
Cuando queremos unir dos tablas que no tienen un campo en comun podemos usar el operador TO_CHAR(NULL),TO_DATE(NULL), y agregamos en el SELECT 
esta sentencia donde indicamos de que tipo es la columna que no tiene la otra tabla en comun
en este caso job_history no contiene los campos de first_name y hire_date y los tenemos que agregar con el operador TO_CHAR(NULL)*/
SELECT hire_date, employee_id, job_id, nvl(first_name,0), nvl(to_date(null),sysdate)
FROM employees
UNION
SELECT TO_DATE(NULL),employee_id, job_id, nvl(TO_CHAR(NULL),0),end_date
FROM job_history
ORDER BY hire_date;



