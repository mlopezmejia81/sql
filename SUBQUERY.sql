/*Estructura*/
SELECT select_list
FROM table
WHERE expression operator
(SELECT select_list
FROM table);


/*En esta consulta decimos que seleccione ciertas columnas de la tabla empleados cuando su fecha de contratacion sea mayor
a la fecha de contratacion de una persona que se apellida VARGAS*/
SELECT first_name, last_name,
hire_date
FROM employees
WHERE hire_date >
(SELECT hire_date
FROM employees
WHERE last_name = 'Vargas' and first_name = 'Peter')
ORDER BY hire_date;

SELECT hire_date, first_name
FROM employees
WHERE last_name = 'Vargas'



/*cuando pasa un error porque hay un null
ya que encuentra a dos personas con el mismo apellido pero y aparte una de ellas tiene un NULL*/
SELECT last_name
FROM employees
WHERE department_id =
(SELECT department_id
FROM employees
WHERE last_name = 'Grant');

SELECT department_id, first_name
FROM employees
WHERE last_name = 'Grant';



SELECT
    round(AVG(salary), 2)
FROM
    employees
WHERE
    department_id = 90; --Redondeo del promedio del salario
SELECT
    last_name
FROM
    employees
WHERE
    salary = (
        SELECT
            MAX(salary)
        FROM
            employees
    );
    
/*SUBQUERY de diferentes tablas*/
SELECT last_name, job_id, department_id
FROM employees
WHERE department_id =
(SELECT department_id
FROM departments
WHERE department_name = 'Marketing')
ORDER BY job_id;

SELECT last_name, job_id, salary, department_id
FROM employees
WHERE job_id =
    (SELECT job_id
    FROM employees
    WHERE employee_id = 141)
AND department_id =
    (SELECT department_id
    FROM departments
    WHERE location_id = 1500);
/*obtiene los empleados cuyo salario esta debajo del promedio de todos los salarios*/    
SELECT last_name, salary
FROM employees
WHERE salary <
(SELECT AVG(salary)
FROM employees)
ORDER BY salary;

/*Agrupa los departamentos, y muestra solo los departamentos cuyo salario minimo(de todos los trabajadores) es mayor al salario minimo 
(de todos los trabajadores) del departamento 50*/
SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) >
(SELECT MIN(salary)
FROM employees
WHERE department_id = 50)
ORDER BY department_id;


