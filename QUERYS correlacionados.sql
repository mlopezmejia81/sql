/*Se usa cuando hacemos referencia a una tabla externa(la misma) en un subquery*/
/*Obtiene el salario de los empleados que es mayor al salario promedio de todos los empleados de su departamento*/
SELECT o.department_id,o.first_name,
o.last_name,
o.salary
FROM employees o
WHERE o.salary >
    (SELECT AVG(i.salary)
    FROM employees i
    WHERE i.department_id =
    o.department_id)
ORDER BY o.department_id;

/*Regresa todos lo que no son managers*/
SELECT last_name AS "Not a Manager"
FROM employees emp
WHERE NOT EXISTS
    (SELECT *
    FROM employees mgr
    WHERE mgr.manager_id = emp.employee_id);
/*dara vacio*/
SELECT last_name AS "Not a Manager"
FROM employees emp
WHERE emp.employee_id NOT IN
(SELECT mgr.manager_id
FROM employees mgr);

/*WITH managers AS
almacena en managers lo que resulte de la consulta*/
WITH managers AS
    (SELECT DISTINCT manager_id
    FROM employees
    WHERE manager_id IS NOT NULL)
SELECT last_name AS "Not a manager"
FROM employees
WHERE employee_id NOT IN
    (SELECT *
    FROM managers);
    
    /**/
