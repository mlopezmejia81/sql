/*Creacion de tablas*/
CREATE TABLE table
(column data type [DEFAULT expression],
(column data type [DEFAULT expression],
(��[ ] );

/*Cuando no estamos conectados a un usuario lo tenemos que indicar en el FROM*/
SELECT *
FROM mary.students;

/**/
CREATE TABLE my_friends
(first_name VARCHAR2(20),
last_name VARCHAR2(30),
email VARCHAR2(30),
phone_num VARCHAR2(12),
birth_date DATE);

CREATE TABLE my_cd_collection
(cd_number NUMBER(3),
title VARCHAR2(20),
artist VARCHAR2(20),
purchase_date DATE DEFAULT SYSDATE);

/*Crear tabalas externas a la base de datos*/
CREATE TABLE emp_load
(employee_number CHAR(5),
employee_dob CHAR(20),
employee_last_name CHAR(20),
employee_first_name CHAR(15),
employee_middle_name CHAR(15),
employee_hire_date DATE)
ORGANIZATION EXTERNAL
(TYPE ORACLE_LOADER
DEFAULT DIRECTORY def_dir1
ACCESS PARAMETERS
(RECORDS DELIMITED BY NEWLINE
FIELDS (employee_number CHAR(2),
employee_dob CHAR(20),
employee_last_name CHAR(18),
employee_first_name CHAR(11),
employee_middle_name CHAR(11),
employee_hire_date CHAR(10) date_format DATE mask
"mm/dd/yyyy"))
LOCATION ('info.dat'));

/*Diccionario de datos*/
SELECT *
FROM USER_TABLES;

SELECT table_name, status
FROM ALL_TABLES;

SELECT *
FROM user_indexes;

/*Zona horarias
13-2 PDF*/
CREATE TABLE time_ex4
(loan_duration1 INTERVAL YEAR(3) TO MONTH,
loan_duration2 INTERVAL YEAR(2) TO MONTH);

INSERT INTO time_ex4 (loan_duration1, loan_duration2)
VALUES (INTERVAL '120' MONTH(3),
INTERVAL '3-6' YEAR TO MONTH);

SELECT SYSDATE + loan_duration1 AS "120 months from now",
SYSDATE + loan_duration2 AS "3 years 6 months from now"
FROM time_ex4;

select *
from time_ex4







