/*SECUENCE*/
/*eSTRUCTURA*/
CREATE SEQUENCE sequence
[INCREMENT BY n]
[START WITH n]
[{MAXVALUE n | NOMAXVALUE}]
[{MINVALUE n | NOMINVALUE}]
[{CYCLE | NOCYCLE}]
[{CACHE n | NOCACHE}];

/**/
CREATE SEQUENCE runner_id_seq
INCREMENT BY 1
START WITH 1
MAXVALUE 50000
NOCACHE
NOCYCLE;

/*Muestra los sequence que tiene el usuario*/
SELECT sequence_name, min_value, max_value, increment_by, last_number
FROM user_sequences;

/*NEXTVAL
Aqui lo que hara es que le asignara el siguiente ID en departaments*/
INSERT INTO departments
(department_id, department_name, location_id)
VALUES (departments_seq.NEXTVAL, 'Support', 2500);

select *
from departments


