/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
/****************SOLVE PROBLEMS ***************/
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/

/*FIRST PROBLEM*/
/*� Problem:
� Create a list of all tables whose first two characters in the name
of the table is JO.
� The tables must be owned by the current Oracle User.*/
SELECT *
from user_tables
WHERE table_name like 'JO%';

/*SECOND*/
/*� Problem:
� Create a list that includes the first initial of
every employee's first name, a space, and
the last name of the employee.*/
SELECT SUBSTR(first_name,1,1) ||' '|| last_name as Union_Name
FROM employees;

/*THIRD*/
/*Problem:
� Create a list of every employee's
first name concatenated to a
space and the employee's last
name, and the email of all
employees where the email
address contains the string 'IN'.*/
SELECT first_name ||' '|| last_name, email
FROM employees
WHERE email LIKE '%IN%';

/*FOURTH*/
/*� Problem:
� Create a list of 'smallest' last
name and the 'highest' last
name from the employees table.*/
SELECT MIN(last_name) as Smallest_Name_Last_Name, MAX(last_name) as Highest_Last_Name
FROM employees;

/*FIFTH*/
/*� Problem:
� Create a list of weekly salaries from
the employees table where the
weekly salary is between 700 and
3000.
� The salaries should be formatted to
include a $-sign and have two
decimal points like: $9999.99.*/
SELECT TO_CHAR((salary/4),'$9999.99') AS WeeKLY_SALARY
FROM employees
WHERE salary/4 BETWEEN 700 AND 3000
ORDER BY salary;

/*SIXTH*/
/*� Problem:
� Create a list of every employee
and his related job title sorted by
job_title. */
SELECT SUBSTR(first_name,1,1) ||' '|| last_name,job_title 
FROM employees NATURAL JOIN jobs ;

/*SEVENTH*/
/* Problem:
� Create a list of every employee's
job, the salary ranges within the
job, and the employee's salary.
� List the lowest and highest
salary range within each job
with a dash to separate the
salaries like this: 100 � 200.*/
SELECT SUBSTR(e.first_name,1,1) ||' '|| e.last_name as Name_Employee,j.job_title, e.salary, e.employee_id
FROM employees e NATURAL JOIN jobs j
ORDER BY salary;


SELECT SUBSTR(first_name,1,1) ||' '|| last_name as Name_Employee, salary,employee_id,
(CASE
WHEN salary BETWEEN 1000 AND 3000 THEN '1000-3000'
ELSE 'N/A'
END) nueva
FROM employees 
ORDER BY salary;


/*EIGHT*/
/*� Problem:
� Using an ANSII join method, create
a list of every employee's first initial
and last name, and department
name.
� Make sure the tables are joined on
all of the foreign keys declared
between the two tables. */
SELECT SUBSTR(first_name,1,1) ||' '|| last_name as Name, departments.department_name
FROM employees JOIN departments USING (department_id);


SELECT last_name,department_id,
CASE department_id
WHEN department_id between 50 and 100 THEN '50-100'
WHEN 80 THEN 'Sales'
WHEN 60 THEN 'It'
ELSE 'Other dept.'
END AS "Department"
FROM employees;

/*NINTH*/
/*� Problem:
� Create a list of every employee's
last name, and the word nobody or
somebody depending on whether
or not the employee has a
manager.
� Use the Oracle DECODE function to
create the list.*/
SELECT last_name, manager_id,
DECODE (manager_id,
null,'nobody',
'somebody') as Works_for
FROM employees;

/*TENTH*/
/*Problem:
� Create a list of every employee's
first initial and last name, salary,
and a yes or no to show whether or
not an employee makes a
commission.
� Fix this query to produce the result.*/
SELECT SUBSTR(first_name,1,1)||' '||last_name "Employee Name", salary "Salary",
DECODE(commission_pct,
null,'no',
'yes') as Commissions,
DECODE(commission_pct, 
NULL, 'No',
'Yes')as Commission
/*DEC(commission_pct NULL, 'No', 'Yes')'Commission'*/
FROM employees;

/*ELEVENTH*/
/*Problem:
� Create a list of every employee's
last name, department name,
city, and state_province.
� Include departments without
employees.
� An outer join is required.*/
SELECT employees.last_name,departments.department_name,locations.city, locations.state_province
FROM employees FULL OUTER JOIN departments 
USING (department_id) FULL OUTER JOIN locations USING(location_id);
   
/*TWELFTH*/
/* Problem:
� Create a list of every employee's first and
last names, and the first occurrence of:
commission_pct, manager_id, or -1.
� If an employee gets commission, display
the commission_pct column; if no
commission, then display his manager_id;
if he has neither commission nor manager,
then the number -1. */
SELECT first_name, last_name,commission_pct,manager_id,
COALESCE (commission_pct,manager_id,-1) "Which Function"
FROM employees
ORDER BY commission_pct;

/*THIRTEENTH*/
/*Problem:
� Create a list of every employee's
last name, salary, and job_grade
for all employees working in
departments with a
department_id greater than 50.*/
SELECT last_name, salary
FROM employees
where department_id>50


/*� 14
Problem:
� Produce a list of every employee's
last name and department name.
� Include both employees without
departments, and departments
without employees.*/
SELECT e.last_name, em.department_name
FROM employees e FULL OUTER JOIN employees em ON (em.department_name=e.department_name)
ORDER BY em.department_id
/*Otra forma de hacerlo*/
SELECT e.last_name, em.department_name
FROM employees e FULL OUTER JOIN departments em ON (em.department_name=e.department_name)
ORDER BY em.department_id


/*Encuentra los datos del manager de un trabajador*/
SELECT em.last_name "Empleado", em.employee_id "ID Empleado", e.last_name "Jefe", em.manager_id "ID Jefe"
FROM employees e FULL OUTER JOIN employees em ON (em.manager_id=e.employee_id)
ORDER BY em.employee_id


/*Regresa los trabajos de cada empelado*/
SELECT em.job_id,em.job_title, e.last_name, e.employee_id
FROM employees e FULL OUTER JOIN jobs em ON (em.job_id=e.job_id)
ORDER BY e.employee_id


/*15
Create a treewalking list of every
employee's last name, his manager's last
name, and his position in the company.
� The top level manager has position 1, this
manager's subordinates position 2, their
subordinates position 3, and so on.
� Start the listing with employee number
100*/
SELECT e.employee_id, e.last_name, e.job_id, e.manager_id,em.job_title
FROM employees e RIGHT OUTER JOIN jobs em ON(e.job_id=em.job_id)
START WITH e.employee_id = 100
CONNECT BY PRIOR e.employee_id = e.manager_id;

SELECT LEVEL, last_name ||
' reports to ' ||
PRIOR last_name
AS "Walk Top Down"
FROM employees
START WITH last_name = 'King'
CONNECT BY PRIOR
employee_id = manager_id;

/*16
Produce a list of the earliest hire date, the latest hire date, and
the number of employees from the employees table*/

SELECT MIN(hire_date) "Lowest", MAX(hire_date) "Highest", COUNT(*)
FROM employees


/*17
� Create a list of department
names and the departmental
costs (salaries added up).
� Include only departments whose
salary costs are between 15000
and 31000, and sort the listing by
the cost.*/

SELECT SUM(salary), department_id
FROM employees 
/*WHERE SUM(salary) BETWEEN 15000 AND 31000*/
GROUP BY department_id
ORDER BY department_id;
