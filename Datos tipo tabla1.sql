/*COLLECTIONS TIPO TABLA Y TIPO ARRAY*/
/*COLECCION DE DATOS*/
/*CREACION DE TYPOS COMO TABLAS*/
CREATE TYPE PARTICIPANT_T AS OBJECT (
  empno   NUMBER(4),
  ename   VARCHAR2(20),
  job     VARCHAR2(12),
  mgr     NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  deptno   NUMBER(2)) 
/

CREATE TYPE MODULE_T  AS OBJECT (
  module_id  NUMBER(4),
  module_name VARCHAR2(20), 
  module_owner REF PARTICIPANT_T, 
  module_start_date DATE, 
  module_duration NUMBER )

/*Es una tabla que a su vez contiene muchos objects
en este caso este tipo MODULETBL_T es una tabla que tiene objects*/
create TYPE MODULETBL_T AS TABLE OF MODULE_T;


CREATE TABLE projects (
  id NUMBER(4),
  name VARCHAR(30),
  owner REF PARTICIPANT_T,
  start_date DATE,
  duration NUMBER(3),
  modules  MODULETBL_T  ) NESTED TABLE modules STORE AS modules_tab ;
  
  
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------*/
/*ARRAY*/
  CREATE TYPE PHONE_ARRAY IS VARRAY (10) OF varchar2(30)
  
  
  /*** Create ADDRESS UDT ***/
CREATE TYPE ADDRESS_2 AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
)
/
/*** Create PERSON UDT containing an embedded ADDRESS UDT ***/
CREATE TYPE PERSON_2 AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    ADDRESS_2
)
/
CREATE TABLE  employees_3
( empnumber            INTEGER PRIMARY KEY,
  person_data     REF  PERSON_2,
  manager         REF  PERSON_2,
  office_addr          address,
  salary               NUMBER,
  phone_nums           phone_array
);
  

