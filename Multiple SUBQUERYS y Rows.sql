/*Aqui nos regresa un error ya que el segundo SELECT nos regresa mas de un registro*/
SELECT first_name, last_name
FROM employees
WHERE salary =
(SELECT salary
FROM employees
WHERE department_id = 20);

/* EXTRACT(YEAR FROM hire_date) 
IN
regresa todos los empleados que fueron contratados en los a;os de los empleados que regrea el segundo SELECT*/
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90);

/*regresa todolos los empleados cuya fecha de contratacion sea menor a la fecha de contratacion del a;o mayor del segundo SELECT*/
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) < ANY
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90 );

/*muestra solo los empleados cuya fecha de contratacion es menor a todos los valores obtenidos del segundo SELECT*/
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) < ALL
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90);

/*Aqui indicamos que nos regrese valores del salario minimo que sea menor al salario minimo de un intervalo de departments_id del segundo SELECT*/
SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) < ANY
(SELECT salary
FROM employees
WHERE department_id IN (10,20))
ORDER BY department_id;


/*Se muestra una consulta escrita de dos formas diferentes, en la primera se muestran las multiples columnas en un subquery
y en la segunda es en 2 subquerys*/
SELECT employee_id, manager_id, department_id
FROM employees
WHERE(manager_id,department_id) IN
    (SELECT manager_id,department_id
    FROM employees
    WHERE employee_id IN (149,174))
AND employee_id NOT IN (149,174);
/*---------------------*/
SELECT employee_id,manager_id,department_id
FROM employees
WHERE manager_id IN
    (SELECT manager_id
    FROM employees
    WHERE employee_id IN(149,174))
AND department_id IN
    (SELECT department_id
    FROM employees
    WHERE employee_id IN(149,174))
AND employee_id NOT IN(149,174);

/*IMPORTANTEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE*/
/*SI ESTAS SEGURO QUE TE VA A REGRESAR UN SOLO REGISTRO EL SUBQUERY, PUEDES USAR EL =, PERO SAI NO ESTAS SEGURO PUEDES USAR EL ANY, IN*/

